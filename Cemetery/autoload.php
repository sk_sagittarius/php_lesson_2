<?php
spl_autoload_register(function ($name)
{
    // PSR-4
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $name);

    $file = "Classes"
        . DIRECTORY_SEPARATOR
        . "{$path}.php";

    if(!file_exists($file) || !is_file($file))
        die("File $file not found");

    include_once $file;

    if(!class_exists($name) and !trait_exists($name) and !interface_exists($name))
        die("Object $name not found");

});