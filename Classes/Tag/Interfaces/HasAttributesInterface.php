<?php

namespace Tag\Interfaces;


use ArrayAccess;

interface HasAttributesInterface extends ArrayAccess
{
    // скопировала из BaseTag
    public function setAttribute($key, $value = null);
    public function appendAttribute($key, $value);
    public function prependAttribute($key, $value);
    // потом перешла в класс BaseTag и добавила 'implements HasAttributesImplements', где надо реализовать методы из ArrayAccess
}

// это все затевается чтобы делать вот так:
// $tag = new Tag();
// $tag['value'] = 'hello';