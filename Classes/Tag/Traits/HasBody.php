<?php
/**
 * Created by PhpStorm.
 * User: Султановак
 * Date: 12.03.2020
 * Time: 21:31
 */

namespace Tag\Traits;


trait HasBody
{
    use CanBeSelfClosing;
    // переписала getBody
    protected $body;
    protected function setBody($body) {
        $this->body = is_array($body) ? $body : [$body];
        return $this;
    }
    public function getBody()
    {
        $body = implode($this->body ?? []);

        if (!method_exists($this, 'isSelfClosing') or !$this->isSelfClosing())
            return $body;
        return null;
    }
    public function appendBody($body) {
        //return $this->setBody($this->getBody() . $body); // deleted
        if(is_array($body))
            foreach ($body as $item)
            {
                $this->appendBody($item);
            }
        else
            $this->body[]=$body;
        return $this;
    }
    public function prependBody($body) {
        //return $this->setBody($body . $this->getBody()); // deleted
        if(is_array($body))
            foreach (array_reverse($body) as $item) // reverse чтоб порядок был правильный
            {
                $this->prependBody($item);
            }
        else
            array_unshift($this->body, $body); // вставили в начало
        return $this;
    }
}