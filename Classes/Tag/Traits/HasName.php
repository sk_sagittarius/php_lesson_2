<?php
/**
 * Created by PhpStorm.
 * User: Султановак
 * Date: 12.03.2020
 * Time: 21:41
 */

namespace Tag\Traits;


trait HasName
{
    protected $name;
    protected function setName($name) {
        $this->name = $name;
        return $this;
    }
    public function getName() {
        return $this->name;
    }
}