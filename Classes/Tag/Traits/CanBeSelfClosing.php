<?php


namespace Tag\Traits;


trait CanBeSelfClosing
{
    // поменяла const на static и $ перед названием $SELF_CLOSING, добавила use HasName для getName

    protected static $SELF_CLOSING = [
        'area', 'base', 'br', 'embed', 'hr', 'iframe', 'img', 'input',
        'link', 'meta', 'param', 'source', 'track'
    ];

    public function isSelfClosing() {
        return in_array($this->getName(), self::$SELF_CLOSING);
    }
}