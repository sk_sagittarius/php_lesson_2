<?php
namespace Tag;
/**
 * Class Form
 * @method self method($value)
 * @method self action($value)
 */
class Form extends NamedTag
{
    protected static function name(): string
    {
        return "form";
    }

    public static function input($name, $type = 'text', $value = null)
    {
        $attributes = [
            'name' => $name,
            'type' => $type
        ];
        if($value)
            $attributes['value'] = $value;
        return Tag::input($attributes);
    }

    public static function label($body, $for = null)
    {
        $label = Tag::label()->appendBody($body);
        if($for)
            $label->setAttribute('for', $for);
        return $label;
    }

    public static function textarea($name, $value = null)
    {
        $attributes = [
            'name' => $name,
        ];
        if($value)
            $attributes['value'] = $value;
        return Tag::textarea($attributes);
    }
    public static function file($name, $type='file', $value = null)
    {
        $attributes = [
            'name' => $name,
            'type' => $type
        ];
        if($value)
            $attributes['value'] = $value;
        return Tag::input($attributes);
    }

    public static function password($name, $type='password', $value = null)
    {
        $attributes = [
            'name' => $name,
            'type' => $type
        ];
        if($value)
            $attributes['value'] = $value;
        return Tag::input($attributes);
    }
}

// label, textarea, password, file