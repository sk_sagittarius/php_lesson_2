<?php

//region SQL queries
//include_once "db_helpers.php";


//$result = mysqli_query($connection, 'select * from `books`');
//
//db_checkOrDie($connection);
//
//$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);


//db_insert('phpdb.books',
//    ['name' => 'some2']);

//db_delete('phpdb.books', ['name' => 'some']);

//db_update('phpdb.books', ['name' => 'SUPERNEW'], ['name' => 'new']);


//$rows = db_select('phpdb.books');
//print_r($rows);
//endregion

//region 05.03.2020
/////////////////////// 05 03 2020
//class MyClass
//{
////    public function foo()
////    {
////        return 'bar' . '<br>';
////    }
//
//    public function __construct($name='')
//    {
//        $this->setName($name);
//    }
//
//    private $name = "John";
//
//    public function getName()
//    {
//        return $this->name;
//    }
//    public function setName($name)
//    {
//        $this->name = $name;
//    }
//
//}
//
//$myClass = new MyClass('First');
//
////echo $myClass->foo(); // вызов функции
////$myClass->setName('Boops');
//echo $myClass->getName(); // вывод строки


//region class Number, +-*/
//class Number
//{
//    private $num = 0;
//
//    public function __construct($num)
//    {
//        $this->num = $num;
//    }
//    public function setNumber($num)
//    {
//        $this->num = $num;
//    }
//    public function getNumber()
//    {
//        if($this->num%2==0)
//            return $this->num*3;
//        return $this->num*2;
//    }
//}
//
//$number = new Number(5);
//echo $number->getNumber();
//echo '<br>';
//$number->setNumber(8);
//echo '<br>';
//echo $number->getNumber();







//class Number
//{
//    private $number;
//    // alt + insert => setter, getter
//    public function __construct($number)
//    {
//        $this->number = $number;
//    }
//
//    public function setNumber($number)
//    {
//        $this->number = $number;
//    }
//    public function getNumber()
//    {
//        return $this->number;
//    }
//    // остальные методы кроме гет и сет должны не напрямую обращаться к числу, а через сеттер и геттер
//
//    public function add($number)
//    {
//        $this->number += $number;
//        return $this->number;
//    }
//    public function sub($number)
//    {
//        $this->number = $this->number - $number;
//        return $this->number;
//    }
//    public function mult($number)
//    {
//        $this->number = $this->number * $number;
//        return $this->number;
//    }
//    public function div($number)
//    {
//        $this->number = $this->number / $number;
//        return $this->number;
//    }
//    public function mod($number)
//    {
//        $this->number = $this->number % $number;
//        return $this->number;
//    }
//}
//
//$number = new Number(5);
//echo $number->getNumber();
//echo '<br>';
//$number->setNumber(8);
//echo '<br>';
//echo $number->getNumber();
//echo '<br>';
//$number->add(5);
//echo $number->getNumber();
//echo '<br>';
//$number->sub(3);
//echo $number->getNumber();
//echo '<br>';
//$number->mult(2);
//echo $number->getNumber();
//echo '<br>';
//$number->div(4);
//echo $number->getNumber();
//echo '<br>';
//$number->mod(3);
//echo $number->getNumber();
//endregion
//endregion


//use ItStep\Room;


//region using Tag
//$tag = new Tag('input');
//$tag->appendBody('new');
//$tag->setAttribute('href', '/');
//
//$tag->__construct('div');
//echo $tag->__toString();
//endregion

//region 10.03.2020
///////// 10.03.2020

//$tag = new Tag('div');
//$tag->addClass('first');
//$tag->addClass('second');
//$tag->removeClass('first');
//print_r($tag->classesAsArray());


// статичный класс
//class Str
//{
//    public static function upper($str)
//    {
//        return mb_strtoupper($str);
//    }
//    public static function slug($str)
//    {
//        $str = str_replace(' ', '-', $str);
//        return self::upper($str);
//    }
//}
//
//echo Str::upper('hello');
//echo Str::slug('Some new');

//class MyStatic
//{
//    public static $counter = 0;
//    public function __construct()
//    {
//        self::incrementCounter();
//    }
//    protected static function incrementCounter()
//    {
//        $file = "counter";
//        if(!file_exists($file) || !is_file($file))
//            self::$counter=0;
//        else
//        {
//            self::$counter = file_get_contents($file);
//        }
//        self::$counter++;
//        file_put_contents($file, self::$counter);
//    }
//}
//
//$a = new MyStatic();
//$b = new MyStatic();
//$c = new MyStatic();
//
//// мой способ
////file_put_contents('text.txt', MyStatic::$counter . "\n", FILE_APPEND);
////$st[] = file_get_contents('text.txt');
////echo count($st);


// константа
//class MyStatic
//{
//    const name = "Some";
//    static $name = "New";
//}
//
//echo MyStatic::$name;

//include_once "Classes/BaseTag.php";

//$link = BaseTag::make('a');
//$link = new Tag('a');
//
//// magic method - __call, __callStatic
//
//// вызовется __call($name, $attributes). Вызывается при отсутствии метода
//$link->id('asd');
//// вызовется __callStatic($name, $attributes). Вызывается при отсутствии static метода
//Tag::div();


// call метод для несуществующих методов
//class Test
//{
//    public function __call($name, $arguments)
//    {
//        var_dump($name); echo "<br>";
//        var_dump($arguments);
//    }
//}
//
//$test = new Test();
//$test->hello('world', 'school');

//echo Tag::input([a => "new"]);


//$link->id('main')->disabled();
//echo $link;
//
//echo Tag::html()->start();
//echo Tag::head()->start();
//
//echo Tag::head()->end();
//echo Tag::html()->end();

//echo Tag::table()->appendBody('hello')->prependBody(['as', '121']);


//$html = Tag::html(['lang'=>'ru']);
//$head = Tag::head()->appendTo($html);
//$body = Tag::body()->appendTo($html);
//$ul = Tag::ul()->appendTo($body);
//$items = ['first', 'second', 'third'];
//foreach ($items as $item) {
//    Tag::li()->appendBody($item)->appendTo($ul);
//}
//
//echo $html;
//endregion


//region 11.03.2020
//////////// 11.03.2020
///
//include_once "autoload.php";

/// наследование
//class GrandParent
//{
//    static $name = "First";
//}
//
//class MyParent extends GrandParent
//{
//    static $name = "Lion";
//    static function getName()
//    {
//        // self вернет только значение из текущего класса, а static переопределенное
//        return static::$name;
//    }
//}
//
//function foo(MyParent $parent)
//{
//    return $parent->getName();
//}
//
//class Child extends MyParent
//{
//    static $name = "Pumba";
//    static function getName()
//    { // parent доступен для статики
//        return parent::$name;
//    }
//}
//
//$child = new Child();
//echo foo($child);


//
//$form = Form::make();
//Form::input('username')->value('login')->appendTo($form);
//$form->input('name');
//echo $form;




////////// пространство имен

// use NAMESPACE\Room
// use NAMESPACE\Room as NewRoom

//$room = new Room();
//echo $room->number;
//endregion


//region 12.03.2020

//region Interface

//region Interface examples
//// интерфейсы могут наследоваться друг от друга
//interface BookInterface
//{
//    public function getName() : string; // должно быть точное имплементирование
//    // только public в интерфейсах, нельзя protected и private. модификатор можем удалить, будет всегда public
//    function setName(string $name) : void;
//}
//interface JournalInterface
//{
//
//}
//interface MagazineInterface extends BookInterface, JournalInterface // наследование
//{
//    function getNumber();
//}
//
//// можем от нескольких интерфейсов имплементироваться, можем от Magazine, который наследуется от двух
//class Book implements BookInterface
//{
//    public function getName() : string
//    {
//        return 'Harry Potter';
//    }
//    function setName(string $name): void
//    {
//        $this->name = $name;
//    }
//}
//class Magazine implements MagazineInterface // три метода имплементируем Book + Magazine
//{
//    public function getName(): string
//    {
//        // TODO: Implement getName() method.
//    }
//    function setName(string $name): void
//    {
//        // TODO: Implement setName() method.
//    }
//    function getNumber()
//    {
//        // TODO: Implement getNumber() method.
//    }
//}
//
//
//// все что подходит под интерфейс book можем вызвать
//function getBookName(BookInterface $book)
//{
//    return $book->getName();
//}
//$book = new Book();
//$magazine = new Magazine();
//getBookName($book);
//getBookName($magazine);
//endregion


//region class Vector as Array
// класс Вектор как Массив с использованием встроенного интерфейса
//class Vector implements ArrayAccess, Iterator // ArrayAccess - существующий интерфейс с 4 методами, Iterator для foreach
//{
//    protected $data = [];
//    private $position = 0;
//    // ArrayAccess
//    public function offsetExists($offset) // offset это ключ $vector['name']
//    {
//        return isset($this->data[$offset]); // true or false, существует или нет
//    }
//    public function offsetGet($offset) // вернуть значение
//    {
//        return $this->data[$offset] ?? null;
//    }
//    public function offsetSet($offset, $value) // задать значение $vector['size'] = 15 // ['offset'] = value
//    {
//        $this->data[$offset] = $value;
//    }
//    public function offsetUnset($offset)
//    {
//        unset($this->data[$offset]);
//    }
//    // наша функция
//    public function toArray()
//    {
//        return $this->data;
//    }
//
//    // Iterator
//    public function current() // возвращает значение по текущей позиции
//    {
//        return $this->offsetGet($this->key());
//    }
//    public function next() // увеличивает итератор
//    {
//        $this->position++;
//    }
//    public function key() // возвращает ключ для ключ/значения (форич)
//    {
//        $keys = array_keys($this->data); // через это потому что ключ может быть именованным
//        return $keys[$this->position]; // вернули ключ
//    }
//    public function valid() // проверка есть ли такой элемент, запускается перед следующей итерацией // 2
//    {
//        return $this->offsetExists($this->key());
//    }
//    public function rewind() // перемотка в начало, перед запуском форич // 1
//    {
//        $this->position = 0;
//    }
//}
//$vector = new Vector();
//$vector['name'] = 'Harry';
//$vector['age'] = '23';
////echo $vector['name']; // вызывается метод offsetExists
////echo array_pop($vector->toArray()); // вектор класс, чтобы работать как с массивом
//foreach ($vector as $key => $item) // пробег foreach по классу
//{
//    //echo "$key => $item<br/>";
//    // в конце форич позиция не перематывается, только перед началом
//}
//endregion


//region my class
//// мой класс чтобы прикрутить проверку. значения только int
//class IntVector extends Vector implements ArrayAccess, Iterator
//{
//    public function offsetSet($offset, $value) // задать значение $vector['size'] = 15 // ['offset'] = value
//    {
//        if(is_numeric($value))
//            $this->data[$offset] = $value;
//        echo "Ащипка"; die;
//    }
//}
//$numbers = new IntVector();
//$numbers['first'] = 'asd'; // error
//endregion

//endregion


//region autoload, tag, trait
//include_once "autoload.php";
// создала папку в Tag Interfaces (нельзя называть в единственном числе Interface), там создала php class HasAttributesInterface
// создание класса - name HasAttributesInterface, namespace Tag\Interfaces, template Interface
// для дальнейших инструкций иди в HasAttributesInterface :)
//endregion

// вернулась из BaseTag
// trait расширение над методами наследования. типа множественного наследования. не баг, а фича

//region trait examples
//abstract class Animal
//{
//    function move() {}
//    function eat() {}
//    function sleep() {}
//}
//
//trait CanFly
//{
//    public function fly(){}
//}
//trait Hunting
//{
//    public function hunt() {}
//}
//trait Swimming
//{
//    public function swim() {}
//}
//
//
//class Eagle extends Animal
//{
//    use CanFly, Hunting;
//}
//class Tiger extends Animal
//{
//    use Hunting;
//}
//class Сolibri extends Animal
//{
//    use CanFly;
//}
//class Shark extends Animal
//{
//    use Hunting, Swimming;
//}
//
//$tomas = new Eagle();
//$tomas->fly();



// trait не наследуется и не наследует
// методы из трейт просто копируются в класс, где юзаются
//trait HasProps
//{
//    private $props=[];
//    public function getProps()
//    {
//        return $this->props;
//    }
//    public function setProps($props)
//    {
//        $this->props = $props;
//    }
//}
//class TestingClass
//{
//    use HasProps
//    {
//        // если есть два конструктора, чтобы не перезаписать
//        // __construct as hasPropConstruct;
//    }
//    public function getProps()
//    {
//        return $this->props; // хотя prop приватная, можно использовать тут, потому что трейт просто копируется
//        // методы и свойства класса приоритетнее trait
//    }
//}
//endregion


// возле Interfaces создаю папку Traits
// в ней создала класс name HasAttributes, namespace Tag\Traits, template Trait
// перешла в BaseTag, все методы для работы с аттрибутами перенести в HasAttribute, кроме AddClass
// в BaseTag добавить use

// также создала HasName, HasBody, CanBeSelfClosing  - по аналогии. не забудь про template

// уфф
// дописали if в autoload.php чтобы была проверка на класс, интерфейс и трейт

// теперича могут делать ТАК:
//echo Tag::div();



//endregion




//region 13.03.2020

include_once "vendor/autoload.php";

// дописала в composer.json
// autoload - autoload_real - autoload_psr4


//endregion
