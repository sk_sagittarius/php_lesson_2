<?php
spl_autoload_register(function ($name)
{
    $file = "./Classes" . DIRECTORY_SEPARATOR ."{$name}.php";

    if(!file_exists($file) || !is_file($file))
        die("File $file not found");

    include_once $file;

    if(!class_exists($name))
        die("Class $name not found");
});

$class = new SomethingElse();